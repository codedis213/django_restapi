from django.conf.urls import url
from django.conf.urls import url, include
from django.views.generic import TemplateView

from rest_framework import routers
from core.views import StudentViewSet, UniversityViewSet

router = routers.DefaultRouter()
router.register(r'students', StudentViewSet)
router.register(r'universities', UniversityViewSet)

urlpatterns = [
    url(r'^docs/', include('rest_framework_swagger.urls')),
]

urlpatterns += router.urls



# urlpatterns = [
#     url(r'^$', TemplateView.as_view(template_name="index.html")),
# ]
